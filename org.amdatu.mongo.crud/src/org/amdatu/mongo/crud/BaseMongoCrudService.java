/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.crud;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.amdatu.mongo.MongoDBService;
import org.amdatu.validator.ValidatorService;
import org.mongojack.DBCursor;
import org.mongojack.JacksonDBCollection;
import org.mongojack.WriteResult;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

/**
 * Base class that CRUD services should extend. This gives all the basic CRUD functionality as specified by the CrudService interface.
 * This is designed as a base class instead of a ready to use service to make it easier to extend with your own methods.
 * Extend this class in your service which should implement your own service interface that should be an extension of CrudService.
 * 
 * WARNING: Make sure to invoke super.start() from your service, otherwise the collections will not be initialized! 
 * 
 */
public abstract class BaseMongoCrudService<T> implements CrudService<T>{
	
	protected final String m_collectionName;
	protected abstract MongoDBService getMongoDbService();
	private Class<T> m_clazz;
	private LoadingCache<String, Optional<T>> m_idCache = CacheBuilder.newBuilder()
		       .maximumSize(1000)
		       .expireAfterWrite(10, TimeUnit.MINUTES)
		       .build(
		           new CacheLoader<String, Optional<T>>() {
		             public Optional<T> load(String id) {
		               return findById(id, false);
		             }
		           });

	
	protected Optional<ValidatorService> getValidatorService() {
		return Optional.empty();
	}
	
	protected volatile JacksonDBCollection<T, String> m_collection;
	
	/**
	 * Constructor to setup the collection and generic type of this CRUD service.
	 * @param collectionName The collection name in Mongo
	 * @param clazz The type to work with in this CRUD service.
	 */
	public BaseMongoCrudService(String collectionName, Class<T> clazz) {
		m_collectionName = collectionName;
		m_clazz = clazz;
	}
	
	public void start() {
		m_collection = JacksonDBCollection.wrap(getMongoDbService().getDB().getCollection(m_collectionName), m_clazz, String.class);
	}
	
	@Override
	public String save(T obj) {
		
		preSave(obj);
		
		if(getValidatorService().isPresent()) {
			try {
				getValidatorService().get().validate(obj);
			} catch(ConstraintViolationException ex) {
				onValidationError(ex.getConstraintViolations());
			}
		}
		
		WriteResult<T, String> save = m_collection.save(obj);
		
		String savedId = save.getSavedId();
		postSave(obj, savedId);
		
		m_idCache.invalidate(savedId);
		
		return savedId;
	}
	
	@Override
	public Optional<T> findById(String id) {
		return findById(id, false);
	}
	
	@Override
	public Optional<T> findById(String id, boolean allowcache) {
		if(allowcache) {
			
			try {
				return m_idCache.get(id);
			} catch (ExecutionException e) {
				throw new RuntimeException(e);
			}
		} else {
			return Optional.ofNullable(m_collection.findOneById(id));
		}
	}
	
	@Override
	public List<T> list() {
		return mapCursorToList(m_collection.find());
	}
	
	@Override
	public void delete(String id) {
		m_collection.removeById(id);
		m_idCache.invalidate(id);
	}
	
	protected void onValidationError(Set<ConstraintViolation<?>> violation) {
		StringBuilder b = new StringBuilder();
		
		violation.stream().map(v -> v.getPropertyPath().toString() + " " + v.getMessage()).forEach(m -> b.append(m + ","));
		
		String errorMessage = b.toString();
		throw new ConstraintViolationException(errorMessage.substring(0, errorMessage.length()-1),violation);
	}
	
	/**
	 * Utility method to take a DBCursor as returned by find methods and transform it to a List. Notice that this will iterate over the full cursor.
	 * @param cursor to make into a list
	 * @return A list containing all results of the cursor.
	 */
	protected List<T> mapCursorToList(DBCursor<T> cursor) {
		List<T> result = new ArrayList<>();
		cursor.forEach(result::add);
		return result;
	}
	
	/**
	 * Hook to invoke custom code before saving an object, such as adding/removing fields.
	 * @param obj Object to save
	 */
	protected void preSave(T obj) {
	}
	
	/**
	 * Hook to invoke custom code after saving an object, such as clearing caches.
	 * @param obj Object to save
	 * @param id Saved id
	 */
	protected void postSave(T obj, String id) {
	}
	
	/**
	 * Hook to invoke custom code after deleting an object, such as clearning caches.
	 */
	protected void postDelete() {
	}
}

