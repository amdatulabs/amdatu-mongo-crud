/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.crud;

import java.util.List;
import java.util.Optional;

/**
 * CRUD service interface that includes common crud operations for generic type T.
 */
public interface CrudService<T> {
	/**
	 * Store or update the given object. An update will happen when the _id field is not null.
	 * @param Object to store
	 * @return The generated id
	 */
	String save(T obj);
	
	/**
	 * Find a specific object by id. Does not allow cached results.
	 * @param Id that should match the ObjectId of the document.
	 * @return An optional containing the found object when exists.
	 */
	public Optional<T> findById(String id);
	
	/**
	 * Find a specific object by id.
	 * @param Id that should match the ObjectId of the document.
	 * @param allowcache Allow results from the cache when available.
	 * @return An optional containing the found object when exists.
	 */
	Optional<T> findById(String id, boolean allowcache);
	
	/**
	 * Return all documents in the collection. Beware of the performance implications for large collections, since this method loads all object in memory.
	 * @return A list contain all objects in the collection.
	 */
	public List<T> list();
	
	/**
	 * Delete a document by id
	 * @param id
	 */
	public void delete(String id);
	
}
