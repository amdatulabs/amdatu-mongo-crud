/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.crud.test.example;

import java.util.List;

import org.amdatu.mongo.MongoDBService;
import org.amdatu.mongo.crud.BaseMongoCrudService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;

/**
 * Example implementation of a data service that uses the BaseMongoCrudService.
 * Note the required invocation of the base class' constructor and start() method to setup the collections and type.
 * The service is required to make a MongoDBService available to the base class. This can be injected as usual.
 *
 */
@Component
public class ExampleCrudServiceImpl extends BaseMongoCrudService<ExampleObject> implements ExampleCrudService {

	public ExampleCrudServiceImpl() {
		super("example", ExampleObject.class);
	}

	@ServiceDependency
	private volatile MongoDBService m_mongoDbService;

	@Start
	public void start() {
		super.start();
	}
	
	@Override
	protected MongoDBService getMongoDbService() {
		return m_mongoDbService;
	}



	@Override
	public List<ExampleObject> findByDescription(String description) {
		return mapCursorToList(m_collection.find().is("description", description));
	}

}
