/*
 * Copyright (c) 2015 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.mongo.crud.test;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.amdatu.testing.mongo.OSGiMongoTestConfigurator.configureMongoDb;
import static org.junit.Assert.*;

import java.util.List;
import java.util.Optional;

import org.amdatu.mongo.MongoDBService;
import org.amdatu.mongo.crud.test.example.ExampleCrudService;
import org.amdatu.mongo.crud.test.example.ExampleObject;
import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.mongodb.BasicDBObject;

public class CrudServiceTest {

	private volatile MongoDBService m_mongoDbService;
	private volatile ExampleCrudService serviceToTest;
	
	@Before
	public void setup() {
				configure(this)
			.add(createServiceDependency().setService(ExampleCrudService.class).setRequired(true))
			.add(configureMongoDb())
            .add(createServiceDependency().setService(MongoDBService.class).setRequired(true))
			.apply();
			}
	
	@Test
	public void testSaveAndFind() {
		String savedId = serviceToTest.save(new ExampleObject("test"));
		Optional<ExampleObject> findById = serviceToTest.findById(savedId);
		assertTrue(findById.isPresent());
	}		
	
	@Test
	public void testFindWithCache() {
		String savedId = serviceToTest.save(new ExampleObject("test"));
		serviceToTest.findById(savedId, true);
		
		//Remove all items from the collection
		m_mongoDbService.getDB().getCollection("example").remove(new BasicDBObject());
		
		Optional<ExampleObject> fromCache = serviceToTest.findById(savedId, true);
		assertTrue(fromCache.isPresent());
	}
	
	@Test
	public void testSpecializedFinder() {
		serviceToTest.save(new ExampleObject("test"));
		List<ExampleObject> found = serviceToTest.findByDescription("test");
		assertFalse(found.isEmpty());
	}
	
	@Test
	public void deleteInvalidatesCache() {
		String savedId = serviceToTest.save(new ExampleObject("test"));
		
		//Warmup cache
		serviceToTest.findById(savedId, true);
		
		serviceToTest.delete(savedId);
		
		Optional<ExampleObject> findFromCache = serviceToTest.findById(savedId, true);
		assertFalse(findFromCache.isPresent());
	}
	
	@Test
	public void delete() {
		String savedId = serviceToTest.save(new ExampleObject("test"));
		
		serviceToTest.delete(savedId);
		
		Optional<ExampleObject> findFromCache = serviceToTest.findById(savedId, false);
		assertFalse(findFromCache.isPresent());
	}
	
	@Test
	public void list() {
		serviceToTest.save(new ExampleObject("test"));
		serviceToTest.save(new ExampleObject("test2"));
		serviceToTest.save(new ExampleObject("test3"));
		
		List<ExampleObject> list = serviceToTest.list();
		assertEquals(3, list.size());
	}
	
	@Test
	public void invalidFindGivesEmptyOptional() {
		Optional<ExampleObject> findFromCache = serviceToTest.findById(new ObjectId().toString(), false);
		assertFalse(findFromCache.isPresent());
	}
	
	@After
	public void after() {
		cleanUp(this);
	}
}
